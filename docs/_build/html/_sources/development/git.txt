=================================================
How to make my app an installable python package?
=================================================

If you have made a few local commits in your git repo,
and if you want to distribute your app, you are on the right page.

You need to clone Bérou, in a different folder, in order to test your changes,
and to be able to continue developing since the following commands will remove
everything in the repository but your application.
::

    git clone https://gitlab.com/Pacodastre/Berou.git
    cd Berou

Then, this command will remove everything from your git repository,
and will just leave your application folder.
::

    # run at root of the repo
    # where example is the name of your app
    git filter-branch --prune-empty --subdirectory-filter berou/my_app/ master

According to the man page, the `--subdirectory-filter` option:

    Only look at the history which touches the given subdirectory.
    The result will contain that directory (and only that) as its project root.
    Implies the section called “Remap to ancestor”.

In order words, it only keeps the commits that were done in the directory, thus, our app.
The `--prun-empty` will remove the empty commits, i.e all the commits made in Bérou,
not in your app.

Make your app an installable python package
-------------------------------------------

Now, we want to make to create a Python package that we can install
i.e add its path to PYTHONPATH.
This will allow us to import it in python like any other python library.
For more information about the way python imports work, you can find some
information on `the python documentation <https://docs.python.org/2/tutorial/modules.html>`_.

Create a directory, with the same name as your app
(the directory has been removed when filtering the git repo).
When the directory has been created, move all the files to it with git.
::

    mkdir app_name
    git mv *.py templates static templatetags locale migrations app_name

Note: if you don't have custom templatetags, nor a locale folder, simply remove them from the list.
Git will not move any file if one file doesn't exist, unlike bash.

Now, we need to create a setup.py, so we can install it.

Here is a template that you can reuse. Adapt it to your app.
::

    #!/usr/bin/env python
    # -*- coding: utf-8 -*-

    import os
    from setuptools import setup

    with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
        README = readme.read()

    # allow setup.py to be run from any path
    os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

    setup(
        name='my_app',
        version='0.1',

        # can import im python code with: import my_app
        packages=['my_app'],

        include_package_data=True,
        license='AGPL License',
        description='My app does lots of things',
        long_description=README,
        url='http://project_url',
        author=u'Your Name',
        author_email='your_email_address@example.com',
        # Adapt if needed
        classifiers=[
            'Environment :: Web Environment',
            'Framework :: Django',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: AGPL License',  # example license
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            'Programming Language :: Python :: 2.7',
            'Topic :: Internet :: WWW/HTTP',
            'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        ],
    )

In addition to the setup.py, we also need a couple of more files.
The first one to create is the README file that is specified in the setup.py.

Create a README.rst, and write a few lines to describe your project.

After that, you'll need two extra files: MANIFEST.in and LICENSE.

in MANIFEST.in:
::

    include LICENSE
    include README.rst
    recursive-include erika/static *
    recursive-include erika/templates *
    recursive-include docs *

In LICENSE, include the content of your license.

Your application is ready to be installed.

Install application
-------------------

Make sure your virtual environment is still activated,
and run the following command:
::

    python setup.py develop

This added your app to the PYTHONPATH,
making it possible to import in the virtualenv python.
Django will be able to import your app when loading it at startup.

You can now remove your app from the Bérou project.
If you're not sure, don't remove your app,
move it to a Bérou root folder.

Run the server, and see if it is Bérou still runs.

Uploading code to Gitlab
------------------------

Create an account on gitlab.com (if you don't have one)
and create a project for your application.

Gitlab will give you a url for your repository.

TODO: Include screenshot
You will need to change the remote repository,
and then you can push to Gitlab.
::

    git remote origin set-url your_git_repo
    git push

You now should have your app available in gitlab.
