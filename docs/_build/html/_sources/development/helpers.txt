=======
Helpers
=======

Bérou comes with a few helpers to help you develop your Bérou applications.
Keep in mind that you do not need these helpers to write an application,
they are just here to help.

mixins
======

Since Bérou is Class-Based Views oriented, Bérou comes with a few mixins that you use in your views.

::

    # -*- coding: utf-8 -*-

    from django.contrib.auth.decorators import login_required
    from django.utils.decorators import method_decorator

    from django.shortcuts import redirect


    class OwnershipRequiredMixin(object):
        """Changes the queryset to contain only the user's links"""

        def get_queryset(self):
            return self.model.objects.filter(user=self.request.user).all()


    class LoginRequiredMixin(object):
        """Ensures that user must be authenticated in order to access view."""

        @method_decorator(login_required)
        def dispatch(self, *args, **kwargs):
            return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


    class LoginExcludedMixin(object):
        """Ensures that user must be authenticated in order to access view."""

        def dispatch(self, *args, **kwargs):
            if self.request.user.is_authenticated():
                return redirect('berou:home')
            return super(LoginExcludedMixin, self).dispatch(*args, **kwargs)

If you want to use any of these, here is a few examples


LoginRequiredMixin
------------------

This mixin is here to prevent anonymous users to access protected pages
(i.e the user needs to be logged in).
The anonymous user will be redirected to the login page.
If the login is successful, the user will be brought back on the page they were trying to access before.

::

    # -*- coding: utf-8 -*-

    from django.views.generic import TemplateView

    from berou.mixins import LoginRequiredMixin


    class MyView(LoginRequiredMixin, TemplateView):
        template_name = 'my_app/my_template.html'


OwnershipRequiredMixin
----------------------

This mixin is here to prevent users to list/edit/delete objects that don't belong to them.

::

    # -*- coding: utf-8 -*-
    from berou.mixins import OwnershipRequiredMixin

    from my_app import models

    class ListItemView(LoginRequiredMixin, OwnershipRequiredMixin, ListView):
        model = models.Item


LoginExcludedMixin
------------------

This mixin does the opposite of `LoginRequiredMixin`.
This page will prevent authenticated users to access a page.

::

    # -*- coding: utf-8 -*-
    from django.views.generic.edit import CreateView

    from berou.mixins import LoginExcludedMixin

    class RegisterUserView(LoginExcludedMixin, CreateView):
        template_name = 'accounts/register_user.html'
        # other attributes/methods here


Templatetags
============

In order to use these templatetags, you will need to load them in your templates.
For each set of templatetags, the import will be given.

Form helpers
------------

Load
^^^^

::

    {% load form_helpers %}

add_class
^^^^^^^^^

This templatetag is a filter to be used on Django form fields. It adds a class, or classes passed in:

.. code-block:: html

    {{ form.my_field|add_class:"class1 class2" }}

add_attribute
^^^^^^^^^^^^^

This templatetag is a filter to be used on Django form fields.
It adds attributes and their values to the generated input tag.
To seperate out the attribute and its value, use the `:` character.

.. code-block:: html

    {{ form.my_field|add_attribute:"data-foo:42" }}

The generated <input> tag will contain an extra attribute: data-foo="42".

This filter can be used in chain in order to add more than one attribute at a time:::

    {{ form.my_field|add_attribute:"data-foo:42"|add_attribute:"data-bar:0" }}


Includes (templates)
====================

Last, but not least, here is a very helpful template that you use in your template in order to render forms.

.. code-block:: html

    <form method="POST">
        {% csrf_token %}
        {% include "berou/includes/field.html" with field=form.my_field %}
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <button class="btn btn-primary" type="submit">{% trans "Register" %}</button>
            </div>
        </div>
    </form>
