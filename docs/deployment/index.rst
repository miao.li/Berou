==========
Deployment
==========

Install applications
====================

To install applications that you are developing, you simply need to run this command:::

    ./manage.py install_app <app name>


If you wish to install an app from the Bérou pypi project, you can install them like this:::

    pip install <app name> --index-url=http://pypi.berou.fr
    ./manage.py install_app <app name>

That's it!

.. toctree::
   :maxdepth: 2

   deployment
   pypi
