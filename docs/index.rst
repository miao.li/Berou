=====
Bérou
=====

Bérou is a platform that comes built-in with a repository
of applications ready to work on it.

.. image:: images/screenshots/home.png
   :alt: Berou
   :align: center


Table of Contents
=================


.. toctree::
   :maxdepth: 3

   development/index
   deployment/index
   usage/index
