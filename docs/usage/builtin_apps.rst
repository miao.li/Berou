=============
Built-in Apps
=============

Buit-in apps are installed, and don't need to be activated by users.
They don't appear in the list of apps and can't be uninstalled,
unless explicitely removed from `berou/settings.py`.

Accounts
========

This application allows users to register, login, logout, and manage their profile.

Registration
------------

Registration allows users to create their accounts.
Without an account, it is not possible to activate/deactivate/use apps.

.. image:: images/screenshots/registration.png
   :alt: Berou
   :align: center

When the registration is complete, you're automatically logged in.
On your home page, you are invited to activate some applications for your profile.

Login
-----

To access your user account, you need to be logged in.
The login form appears in the header bar, at the top of the page.
The login form is present on all the pages, so you can log in at any time.
If you are trying to access a page that needs the user to be logged in,
you will be redirected to a login page. However, after loging in,
you'll be redirected to the page you were trying to access previously.

.. image:: images/screenshots/login.png
   :alt: Berou
   :align: center

Logout
------

The logout button is located in the user menu, at the top-right corner.
Click on your username to expand the menu.

Note: Javascript must be enabled to open the menu.
A link to logout is also available in the footer if you do not have javasript enabled.

.. image:: images/screenshots/user_menu.png
   :alt: Berou
   :align: center

Manage profile
--------------

Display profile
^^^^^^^^^^^^^^^

You can access your profile by clicking on the profile link located in the user menu.
This page shows all your details.

Note: A part from the username/password, the information is not used at all.
It is only used on the profile page, which is not protected (i.e it is a public page)


.. image:: images/screenshots/user_profile.png
   :alt: Berou
   :align: center

Edit profile
^^^^^^^^^^^^

The edit profile button is available at the top-right corner of the profile panel.
You can edit your personal details on this page.

Note: A part from the username/password, the information is not used at all.
It is only used on the profile page, which is not protected (i.e it is a public page)

.. image:: images/screenshots/edit_user_profile.png
   :alt: Berou
   :align: center

Change password
^^^^^^^^^^^^^^^

It is possible for users to change their passwords
by clicking on the 'Change password' link on the user profile.

.. image:: images/screenshots/change_password.png
   :alt: Change password form
   :align: center

Application System
==================

The application system is a built-in application,
that allows user to activate/deactivte applications for their profile.
The home page is a list of applications that the user has enabled,
in order to use them.

.. image:: images/screenshots/home.png
   :alt: Berou
   :align: center

You can access the list of applications that you want to use in the `Manage my apps`
link available in the user menu (located in the top-right corner).
This page shows the list of applications available.
It shows the logo, the name and the description of the application.
The user can enable/disable any one of them.

.. image:: images/screenshots/manage_my_apps.png
   :alt: Berou
   :align: center
