===========
Development
===========

About Bérou apps
================

At the moment, all the apps in the Bérou are using `Django Class-Based Views <https://docs.djangoproject.com/en/1.8/topics/class-based-views/>`_.

For a complete tutorial on how to create django apps, we invite you to read the `official tutorial <https://docs.djangoproject.com/en/1.8/intro/tutorial01/>`_.

`A good reference for Django Class-Based Views <http://ccbv.co.uk/>`_

Reports bugs on `Gitlab <https://gitlab.com/Pacodastre/Berou/issues>`_.

.. toctree::
   :maxdepth: 2

   install
   git
   helpers
   i18n
   tutorial
