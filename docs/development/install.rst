Installation
============

::

    # Get the source code
    git clone https://gitlab.com/Pacodastre/Berou.git

    # Create a virtualenv for the project
    mkvirtualenv berou

    # cd into the project
    cd Berou

    # Install dependencies using pip
    pip install -r berou/requirements.txt

    # cd into the django project
    cd berou

    # Create the database
    ./manage.py migrate

After installing the software, you might to create a superuser to access the admin interface.

::

    ./manage.py createsuperuser


Running the project
===================

Start the server with the following command:

::

    ./manage.py runserver
