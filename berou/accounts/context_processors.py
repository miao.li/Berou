# -*- coding: utf-8 -*-

"""
Contains function that returns dictionaries that are passed in templates.

The context processors are used in `berou/settings.py`.
The path of this fie is referenced in TEMPLATES settings.
"""

from accounts import views


def header(request):
    """ This context_processor is used to inject the login form
    to all templates, avoiding the need to provide it each view. """

    login_view = views.LoginUserView()
    login_view.request = request
    context = {
        'login_form': login_view.get_form(),
    }
    return context
