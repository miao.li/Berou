# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('content', models.CharField(default=b'', max_length=1000)),
                ('pub_date', models.DateTimeField(default=datetime.datetime(2015, 5, 5, 14, 14, 57, 803793, tzinfo=utc))),
            ],
        ),
    ]
