from django.db import models
from django.utils import timezone
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _


class Post(models.Model):
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=1000, default='')
    pub_date = models.DateTimeField(default=timezone.now)

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        default=None,
        verbose_name=_('User')
    )

    def get_absolute_url(self):
        return reverse('blog')

    def __str__(self):
        return self.title
