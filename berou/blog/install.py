# -*- coding: utf-8 -*-

from application_system import models


def install():
    app = models.App(
        name=__package__.capitalize(),
        description='Post your blog',
        slug=__package__
    )
    app.save()