from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, ListView, TemplateView
from blog import models
# Create your views here.


class CreatePostView(CreateView):
    model = models.Post
    template_name = 'blog/create_post.html'
    fields = ['title', 'content']


class ListPostView(ListView):
    model = models.Post
    template_name = 'blog/list_post.html'


class Test(TemplateView):
    template_name = 'blog/test.html'
