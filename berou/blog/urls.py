from django.conf.urls import url
from blog import views

urlpatterns = [
    url(r'^create$', views.CreatePostView.as_view(), name='create'),
    url(r'^$', views.ListPostView.as_view(), name='list'),
    url(r'^test$', views.Test.as_view(), name='list'),

    # url(r'^$', views.ListPostView.as_view(), name='list'),
 ]
