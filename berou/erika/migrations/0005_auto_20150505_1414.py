# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('erika', '0004_link_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='link',
            name='modification_datetime',
            field=models.DateTimeField(default=datetime.datetime(2015, 5, 5, 14, 14, 57, 802639, tzinfo=utc)),
        ),
    ]
