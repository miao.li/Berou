# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('application_system', '0009_app_logo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='app',
            name='slug',
        ),
        migrations.AddField(
            model_name='app',
            name='package',
            field=models.CharField(default=None, unique=True, max_length=200, verbose_name='Package name'),
        ),
        migrations.AlterField(
            model_name='app',
            name='description',
            field=models.TextField(verbose_name='Description'),
        ),
    ]
