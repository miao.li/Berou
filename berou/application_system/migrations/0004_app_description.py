# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('application_system', '0003_auto_20150504_1319'),
    ]

    operations = [
        migrations.AddField(
            model_name='app',
            name='description',
            field=models.CharField(default=b'', max_length=200),
        ),
    ]
