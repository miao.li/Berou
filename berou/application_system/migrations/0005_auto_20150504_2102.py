# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('application_system', '0004_app_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='app',
            name='slug',
            field=models.SlugField(default=None, max_length=200),
        ),
        migrations.AlterField(
            model_name='app',
            name='name',
            field=models.CharField(max_length=200),
        ),
    ]
