# -*- coding: utf-8 -*-

import os

from django.conf import settings
from django.core.management.commands.startapp import Command as StartApp


class Command(StartApp):
    def handle(self, *args, **options):
        options['template'] = settings.BEROU_DEFAULT_TEMPLATE
        app_name = options['name']
        target = os.path.join(settings.BEROU_APPS_DIR, app_name)
        os.mkdir(target)
        super(StartApp, self).handle('app', target=target, *args, **options)
