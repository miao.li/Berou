# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.views.generic import TemplateView

from berou import views

urlpatterns = [
    url(r'^$', views.ListAppView.as_view(), name='home'),
    url(r'^about/$',
        TemplateView.as_view(template_name='berou/about.html'),
        name='about')
]
