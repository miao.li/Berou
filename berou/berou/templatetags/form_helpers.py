#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import template
register = template.Library()


@register.filter(name='add_class')
def add_class(field, css):
    """
    Example: {{ form.field|add_class:"form-control" }}
    """
    field.field.widget.__dict__['attrs']['class'] = css
    return field


@register.filter(name='add_attribute')
def add_attribute(field, filter_value):
    """
    Example: {{ form.field|add_attribute:"placeholder:John Madigan" }}
    """
    attribute, value = filter_value.split(':', 1)
    field.field.widget.__dict__['attrs'][attribute] = value
    return field
