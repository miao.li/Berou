# -*- coding: utf-8 -*-

from django import template
from django.template.loader import render_to_string

from application_system import models

register = template.Library()


@register.simple_tag
def banner(request, user):
    list_app = [app for app in models.App.objects.all()
                if user in app.users.all()]
    return render_to_string(
        'berou/banner.html',
        {
            'list_app': list_app,
            'request': request,
        }
    )


@register.filter
def path_startswith(string1, string2):
    return string1.startswith('/'+string2)
